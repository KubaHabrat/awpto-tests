public class Bet {
    int chips;

    public Bet(int chips) {
        this.chips = chips;
    }

    public int getChips() {
        return chips;
    }

    public void setChips(int chips) {
        this.chips = chips;
    }
}
