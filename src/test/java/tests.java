import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.HeaderConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

public class tests {

    public static RequestSpecification spec;
    Player player;

    @BeforeAll
    public static void initSpec() {

        spec = new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setBaseUri("https://roulette-api.nowakowski-arkadiusz.com/")
                .addHeader("Authorization", "")
                .addFilter(new RequestLoggingFilter())
                .addFilter(new ResponseLoggingFilter())
                .build();
    }

    private static Stream<Arguments> cornersProvider() {
        List<String> corners = new ArrayList<>();
        corners.add("0-1-2-3");
        for (int i = 0; i < 32; i++) {
            if ((i + 1) % 3 != 0) {
                corners.add(i + 1 + "-" + (i + 2) + "-" + (i + 4) + "-" + (i + 5));
            }
        }
        List<Arguments> arguments = new ArrayList<>();
        for (String corner : corners) {
            arguments.add(Arguments.of(corner));
        }
        return arguments.stream();
    }

    private static Stream<Arguments> linesProvider() {
        List<String> lines = new ArrayList<>();
        for (int i = 0; i < 31; i += 3) {
            lines.add((i + 1) + "-" + (i + 2) + "-" + (i + 3) + "-" + (i + 4) + "-" + (i + 5) + "-" + (i + 6));
        }
        List<Arguments> arguments = new ArrayList<>();
        for (String line : lines) {
            arguments.add(Arguments.of(line));
        }
        return arguments.stream();
    }

    private static Stream<Arguments> streetProvider() {
        List<String> streets = new ArrayList<>();
        streets.add("0-1-2");
        streets.add("0-2-3");
        for (int i = 0; i < 34; i += 3) {
            streets.add((i + 1) + "-" + (i + 2) + "-" + (i + 3));
        }
        List<Arguments> arguments = new ArrayList<>();
        for (String street : streets) {
            arguments.add(Arguments.of(street));
        }
        return arguments.stream();
    }

    private static Stream<Arguments> splitProvider() {
        List<String> splits = new ArrayList<>();
        for (int i = 0; i < 35; i++) {
            splits.add((i) + "-" + (i + 1));
        }
        List<Arguments> arguments = new ArrayList<>();
        for (String split : splits) {
            arguments.add(Arguments.of(split));
        }
        return arguments.stream();
    }

    @BeforeEach
    public void setPlayer() {
        this.player = given()
                .spec(spec)
                .when()
                .post("players")
                .then()
                .extract().as(Player.class);
        HeaderConfig headerConfig = new HeaderConfig();
        spec.config(RestAssuredConfig.config().headerConfig(headerConfig.overwriteHeadersWithName("Authorization")))
                .header("Authorization", player.getHashname());
    }

    @Test
    public void newPlayerShouldHave100Chips() {
        int chips = given()
                .spec(spec)
                .when()
                .get("chips")
                .then()
                .extract().jsonPath().getInt("chips");
        assertThat(chips).isEqualTo(100);
    }

    @ParameterizedTest
    @ValueSource(ints = {2, 4, 6, 8, 10, 11, 13, 15, 17, 19, 20, 22, 24, 26, 29, 31, 33, 35})
    public void shouldWinChipsAfterHittingOnBlacks(int number) {
        bet("black", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(200);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 3, 5, 7, 9, 12, 14, 16, 18, 21, 23, 25, 27, 28, 30, 32, 34, 36})
    public void shouldLoseChipsAfterMissingOnBlacks(int number) {
        bet("black", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(0);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 3, 5, 7, 9, 12, 14, 16, 18, 21, 23, 25, 27, 28, 30, 32, 34, 36})
    public void shouldWinChipsAfterHittingOnReds(int number) {
        bet("red", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(200);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 2, 4, 6, 8, 10, 11, 13, 15, 17, 19, 20, 22, 24, 26, 29, 31, 33, 35})
    public void shouldLoseChipsAfterMissingOnReds(int number) {
        bet("red", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(0);
    }

    @ParameterizedTest
    @ValueSource(ints = {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36})
    public void shouldWinChipsAfterHittingOnEvens(int number) {
        bet("even", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(200);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35})
    public void shouldLoseChipsAfterLosingOnEvens(int number) {
        bet("even", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(0);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35})
    public void shouldWinChipsAfterHittingOnOdds(int number) {
        bet("odd", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(200);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36})
    public void shouldLoseChipsAfterLosingOnOdds(int number) {
        bet("odd", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(0);
    }

    @ParameterizedTest
    @ValueSource(ints = {19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36})
    public void shouldWinChipsAfterHittingOnHighs(int number) {
        bet("high", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(200);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18})
    public void shouldLoseChipsAfterLosingOnHighs(int number) {
        bet("high", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(0);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18})
    public void shouldWinChipsAfterHittingOnLows(int number) {
        bet("low", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(200);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36})
    public void shouldLoseChipsAfterLosingOnLows(int number) {
        bet("low", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(0);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12})
    public void shouldWinChipsAfterHittingOn1stDozen(int number) {
        bet("dozen/1", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(300);
    }

    @ParameterizedTest
    @ValueSource(ints = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13})
    public void shouldWinChipsAfterHittingOn2ndDozen(int number) {
        bet("dozen/2", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(300);
    }

    @ParameterizedTest
    @ValueSource(ints = {3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14})
    public void shouldWinChipsAfterHittingOn3rdDozen(int number) {
        bet("dozen/3", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(300);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36})
    public void shouldLoseChipsAfterMissingOn1stDozen(int number) {
        bet("dozen/1", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(0);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36})
    public void shouldLoseChipsAfterMissingOn2ndDozen(int number) {
        bet("dozen/2", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(0);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36})
    public void shouldLoseChipsAfterMissingOn3rdDozen(int number) {
        bet("dozen/3", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(0);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34})
    public void shouldWinChipsAfterHittingOn1stColumn(int number) {
        bet("column/1", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(300);
    }

    @ParameterizedTest
    @ValueSource(ints = {2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35})
    public void shouldWinChipsAfterHittingOn2ndColumn(int number) {
        bet("column/2", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(300);
    }

    @ParameterizedTest
    @ValueSource(ints = {3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36})
    public void shouldWinChipsAfterHittingOn3rdColumn(int number) {
        bet("column/3", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(300);
    }

    @ParameterizedTest
    @ValueSource(ints = {2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36})
    public void shouldLoseChipsAfterMissingOn1stColumn(int number) {
        bet("column/1", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(0);
    }

    @ParameterizedTest
    @ValueSource(ints = {2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36})
    public void shouldLoseChipsAfterMissingOn2ndColumn(int number) {
        bet("column/2", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(0);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34, 2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35})
    public void shouldLoseChipsAfterMissingOn3rdColumn(int number) {
        bet("column/3", 100);
        spin(number);
        assertThat(getChips()).isEqualTo(0);
    }

    @DisplayName("Should return correct amount of chips after betting on corner")
    @ParameterizedTest(name = "[{index}] Corner: {0}")
    @MethodSource("cornersProvider")
    public void shouldReturnCorrectAmountOfChipsAfterBettingOnCorner(String corner) {
        List<Integer> cornerValues = Arrays.stream(corner.split("-")).map(Integer::parseInt).collect(Collectors.toList());
        for (int i = 0; i < 37; i++) {
            int currentChips = getChips();
            bet("corner/" + corner, 100);
            spin(i);
            if (cornerValues.contains(i)) {
                assertThat(getChips()).as("Betting on " + i).isEqualTo(currentChips + 800);
            } else {
                assertThat(getChips()).as("Betting on " + i).isEqualTo(currentChips - 100);
            }
        }
    }

    @DisplayName("Should return correct amount of chips after betting on line")
    @ParameterizedTest(name = "[{index}] Line: {0}")
    @MethodSource("linesProvider")
    public void shouldReturnCorrectAmountOfChipsAfterBettingOnLine(String line) {
        List<Integer> lineValues = Arrays.stream(line.split("-")).map(Integer::parseInt).collect(Collectors.toList());
        for (int i = 0; i < 37; i++) {
            int currentChips = getChips();
            bet("line/" + line, 100);
            spin(i);
            if (lineValues.contains(i)) {
                assertThat(getChips()).as("Betting on " + i).isEqualTo(currentChips + 500);
            } else {
                assertThat(getChips()).as("Betting on " + i).isEqualTo(currentChips - 100);
            }
        }
    }

    @DisplayName("Should return correct amount of chips after betting on street")
    @ParameterizedTest(name = "[{index}] Street: {0}")
    @MethodSource("streetProvider")
    public void shouldReturnCorrectAmountOfChipsAfterBettingOnStreet(String street) {
        List<Integer> streetValues = Arrays.stream(street.split("-")).map(Integer::parseInt).collect(Collectors.toList());
        for (int i = 0; i < 37; i++) {
            int currentChips = getChips();
            bet("street/" + street, 100);
            spin(i);
            if (streetValues.contains(i)) {
                assertThat(getChips()).as("Betting on " + i).isEqualTo(currentChips + 1100);
            } else {
                assertThat(getChips()).as("Betting on " + i).isEqualTo(currentChips - 100);
            }
        }
    }

    @DisplayName("Should return correct amount of chips after betting on split")
    @ParameterizedTest(name = "[{index}] Split: {0}")
    @MethodSource("splitProvider")
    public void shouldReturnCorrectAmountOfChipsAfterBettingOnSplit(String street) {
        List<Integer> streetValues = Arrays.stream(street.split("-")).map(Integer::parseInt).collect(Collectors.toList());
        for (int i = 0; i < 37; i++) {
            int currentChips = getChips();
            bet("split/" + street, 100);
            spin(i);
            if (streetValues.contains(i)) {
                assertThat(getChips()).as("Betting on " + i).isEqualTo(currentChips + 1700);
            } else {
                assertThat(getChips()).as("Betting on " + i).isEqualTo(currentChips - 100);
            }
        }
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36})
    public void shouldReturnCorrectAmountOfChipsAfterBettingOnStraight(int straight) {
        for (int i = 0; i < 37; i++) {
            int currentChips = getChips();
            bet("straight/" + straight, 100);
            spin(i);
            if (straight == i) {
                assertThat(getChips()).as("Betting on " + i).isEqualTo(currentChips + 3500);
            } else {
                assertThat(getChips()).as("Betting on " + i).isEqualTo(currentChips - 100);
            }
        }
    }

    private void spin(int number) {
        given()
                .spec(spec)
                .when()
                .post("spin/" + number);
    }

    private void bet(String bet, Integer chips) {
        given()
                .spec(spec)
                .body(new Bet(chips))
                .when()
                .post("bets/" + bet)
        ;
    }

    private int getChips() {
        return given()
                .spec(spec)
                .when()
                .get("chips")
                .then()
                .extract().jsonPath().getInt("chips");
    }

}

